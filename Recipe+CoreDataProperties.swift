//
//  Recipe+CoreDataProperties.swift
//  Cook with Me
//
//  Created by Mehdi Gilanpour on 7/1/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Recipe {

    @NSManaged var image: NSData?
    @NSManaged var title: String?
    @NSManaged var ingredients: String?
    @NSManaged var steps: String?

}
