//
//  ViewController.swift
//  Cook with Me
//
//  Created by Mehdi Gilanpour on 7/1/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var TableView: UITableView!
    var recipes = [Recipe]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        TableView.delegate = self
        TableView.dataSource = self
        
    }
    
    override func viewDidAppear(animated: Bool) {
        setAndFetchResults()
        TableView.reloadData()
    }
    
    //Talk to Core Data inorder to getting data
    func setAndFetchResults(){
    
    	let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Recipe")
        
        do{
        
            let results = try context.executeFetchRequest(fetchRequest)
            self.recipes = results as! [Recipe]
            
        }catch{}
    
    }

    
    //Codes about making dynamic cell
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = TableView.dequeueReusableCellWithIdentifier("RecipeCell") as? TableViewCell{
        
            let recipe = recipes[indexPath.row]
            cell.configCell(recipe)
            return cell
        
        }
        return TableViewCell()
        
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }
    
    //Delete cell
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if editingStyle == .Delete
        {
            let app = UIApplication.sharedApplication().delegate as! AppDelegate
            let context = app.managedObjectContext
            context.deleteObject(recipes.removeAtIndex(indexPath.row))
            do{
                
                try context.save()
                
            }catch{}
            

            //recipes.removeAtIndex(indexPath.row)
            self.TableView.reloadData()
        }
    }
}

