//
//  TableViewCell.swift
//  Cook with Me
//
//  Created by Mehdi Gilanpour on 7/1/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var RecipeImage: UIImageView!
    
    
    @IBOutlet weak var showMore: UIButton!
    
    var rec:Recipe!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configCell(recipe: Recipe){
    
        RecipeImage.image = recipe.getImage()
        showMore.setTitle(recipe.title, forState: UIControlState.Normal)
        self.rec = recipe
    }

    @IBAction func show(sender: UIButton) {
        
        File.manager.title = rec.title!
        File.manager.steps = rec.steps!
        File.manager.image = rec.getImage()
        File.manager.ig = rec.ingredients!
        
    }
    
}
