//
//  ViewController2.swift
//  Cook with Me
//
//  Created by Mehdi Gilanpour on 7/1/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit
import CoreData

class ViewController2: UIViewController , UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate{

    @IBOutlet weak var Recipeingredients: UITextField!
    @IBOutlet weak var RecipeTitle: UITextField!
    @IBOutlet weak var RecipeSteps: UITextField!
    @IBOutlet weak var RecipeImage: UIImageView!
    
    var imagePicker : UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        RecipeImage.layer.cornerRadius = 10.0
        RecipeImage.clipsToBounds = true
        
        RecipeSteps.delegate = self
        Recipeingredients.delegate = self
        RecipeTitle.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
            imagePicker.dismissViewControllerAnimated(true, completion: nil)
            RecipeImage.image = image
    }

    @IBAction func addImage(sender: UIButton) {
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    //Save data to Core Data
    @IBAction func CreatRecipe(sender: UIButton) {
        
        if let title = RecipeTitle.text where title != "" {
        
            let app = UIApplication.sharedApplication().delegate as! AppDelegate
            let context = app.managedObjectContext
            let entity = NSEntityDescription.entityForName("Recipe", inManagedObjectContext: context)
            let recipe = Recipe(entity: entity! , insertIntoManagedObjectContext: context)
            recipe.title = title
            recipe.steps = RecipeSteps.text
            recipe.setRecipeImage(RecipeImage.image!)
            recipe.ingredients = Recipeingredients.text
            
            
            
            context.insertObject(recipe)
            
            do{
            
                try context.save()
                
            }catch{}
            
            view.endEditing(true)
            self.navigationController?.popViewControllerAnimated(true)
        
        }
        
    }
    
    
    //Keyboard Handling
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
    
}
