//
//  ViewController3.swift
//  Cook with Me
//
//  Created by Mehdi Gilanpour on 7/1/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {

    @IBOutlet weak var stepts: UILabel!
    @IBOutlet weak var ig: UILabel!
    @IBOutlet weak var titleBar: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        image.layer.cornerRadius = 10.0
        image.clipsToBounds = true
        
        stepts.text = ""
        ig.text = ""
        titleBar.text = ""
        image.image = UIImage(named: "empty")
    }

    override func viewDidAppear(animated: Bool) {
        stepts.text = File.manager.steps
        ig.text = File.manager.ig
        titleBar.text = File.manager.title
        image.image = File.manager.image
    }

}
