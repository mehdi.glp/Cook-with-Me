//
//  Recipe.swift
//  Cook with Me
//
//  Created by Mehdi Gilanpour on 7/1/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Recipe: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    func setRecipeImage(image: UIImage){
    
    		let data = UIImagePNGRepresentation(image)
            self.image = data
    
    }

    func getImage() -> UIImage{
    
    	let img = UIImage(data: self.image!)
        return img!
    
    }
    
}
